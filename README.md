# ReactTemplate

###Getting Started###

Checkout this repo, install depdencies, then start the gulp process with the following:

```
	> git clone git@gitlab.com:dakle/ReactTemplate.git
	> cd ReactTemplate
	> npm install
	> npm start
```